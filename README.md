# README #

This is a sample web app showing how to create custom errors for your Aerobatic hosted web app.

The web app can be viewed at [http://custom-errors.aerobatic.io/](http://custom-errors.aerobatic.io/)